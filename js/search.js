$(function() {
	clear();

	//////////////////////////////////////////////
	// サーバとの接続処理
	//////////////////////////////////////////////

//	var ws = new WebSocket("ws://localhost:8686");
var ws = io.connect("http://10.201.10.149:8008");

//websocket設定
ws.socket.options.reconnect =false;
ws.socket.options["connect timeout"] = Infinity;
ws.socket.options["sync disconnect on unload"] = false;
ws.socket.options["transports"].splice(1,4);

	//////////////////////////////////////////////
	// メッセージ受信時の処理
	//////////////////////////////////////////////
/*
	ws.onopen = function() {
		console.log("Connected");
		ws.send(JSON.stringify({
                                        type:"auth",
                                        location:window.location.href
                                }));
         $("#logs").text("認証中");
         $("#searchButton").attr('disabled', 'disabled');
	};
*/
	
/*	
	ws.on('connect', function() {
		console.log("Connected");
		ws.send(JSON.stringify({
					type:"auth",
					location:window.location.href
		}));
		$("#logs").text("認証中");
		$("#searchButton").attr('disabled', 'disabled');
	});
*/
	ws.on("message",function(data){
		data = JSON.parse(data);
		console.log(data);
		if(wsFunction[data.type] != undefined){                                                                                                            
			wsFunction[data.type](data);
		}else{
			var obj = new Object();
			obj.data = data;
			$(document).trigger("wsEvent",[obj]);
		 }   
	});



/*

	ws.onmessage = function(event) {
		var wsdata = $.parseJSON(event.data);

		if(wsFunction[wsdata.type] != undefined){
			wsFunction[wsdata.type](wsdata);
		}
	}
*/

	var wsFunction = {
		//接続メッセージ
		con:function(data){
				
				console.log("Connected");
				ws.send(JSON.stringify({
							type:"auth",
							location:window.location.href
				}));
				$("#logs").text("認証中");
				$("#searchButton").attr('disabled', 'disabled');


				ws.send(JSON.stringify({
					type:"auth",
					location:window.location.href
				}));

			},
		//認証リダイレクト
		auth:function(data){
			if(data.auth == false){
			location.replace(data.href);
			}
		},
		searchResult:function(data){
			//console.log(data);
			
			if(data.data.length==0){
				$("#logs").text("検索結果０でした");
			}
			
			console.log(data);
		//$("#logs").text(data);
		$("#searchButton").removeAttr('disabled')
		for(var i = 0 ; i < data.data.length ; i++){
			//console.log(data.data[i].id)
			//console.log(data.data[i].address);
			for(var j = 0 ; j < data.data[i].address.length ; j++){
					if(i==0 && j==0){$("#logs").text(data.data[i].address[j]);}
					else{$("<div>").text(data.data[i].address[j]).prependTo("#logs");}
			}
		}	
		},
		authResult:function(data){
			console.log(data.data);
		//$("<div>").text("認証が通ったので、検索可能です。").prependTo("#logs");
		$("#logs").text("認証が通ったので、検索可能です。");
		$("#searchButton").removeAttr('disabled')
		}
	}
	
	
	
	$('#searchButton').click(function(){
		$("#logs").text("検索中");
		ws.send(JSON.stringify({
					type:"search",
					data:$("#message").val()
				}));
		$("#searchButton").attr('disabled', 'disabled');
		clear();

})
	
/*
	//////////////////////////////////////////////
	// Search ボタンがクリックされたときの処理
	// (メッセージ送信時の処理)
	//////////////////////////////////////////////
	$("#search").submit(function() {

		if ($("#message").val()) {
			
			ws.send(JSON.stringify({
					type:"search",
					data:$("#message").val()
				}));
		}

		clear();
		return false;
	});
*/

	function clear() {
		$("#message").val('').focus();
	}

});
