$(function() {
clear();

var ws = io.connect("http://localhost:8008");

//websocket設定
ws.socket.options.reconnect =false;
ws.socket.options["connect timeout"] = Infinity;
ws.socket.options["sync disconnect on unload"] = false;
ws.socket.options["transports"].splice(1,4);




ws.on('connect', function(){
	if(window.location.search==undefined){
		//codeがない場合
		$(".notloginpage").show();
	}else{
		//codeがある場合
		code();
		$(".bar").css("width","50%");
		$(".notloginpage").hide();
		$(".inprogresspage").show();	
	}
});

ws.on("message",function(data){
	console.log(data);
	if(wsFunction[data.type] != undefined){                                               
		wsFunction[data.type](data);
	}else{
	}   
});

$(".loginwithgoogle").click(function(){
	code();
});

var wsFunction = {
	auth_ok:function(data){
		$(".bar").css("width","100%");
		setTimeout(function(){
			$(".accountname").text("Logined");
			$(".inprogresspage").hide();
			$(".login").hide();
			$(".loginpage").show();
			$(".setting").show();	
			$(".logout").show();		
		},1000);
	},
	getCode:function(data){
		$(".bar").css("width","50%");
		location.replace(data.url);
	}
}

var code = function(){
	$(".notloginpage").hide();
	$(".inprogresspage").show();	
	ws.emit('message',{
			type:"connected",
			url:window.location.href
	});
}

function clear() {
	        $("#message").val('').focus();
}

});
