$(function() {
	clear();

	var ws = io.connect("http://localhost:8008");

	//websocket設定
	ws.socket.options.reconnect =false;
	ws.socket.options["connect timeout"] = Infinity;
	ws.socket.options["sync disconnect on unload"] = false;
	ws.socket.options["transports"].splice(1,4);


	ws.on('connect', function(){
		ws.emit('message',{
			type:"connected",
			url:window.location.href
		});
		$("#logs").text("認証中");                                            
		$("#searchButton").attr('disabled', 'disabled');
	});

	ws.on("message",function(data){
		console.log(data);
		if(wsFunction[data.type] != undefined){                                               
			wsFunction[data.type](data);
		}else{
		}   
	});

	var wsFunction = {
		docsList:function(data){
			$("#logs").text("ようこそ！"+data.data.author[0].name['$t']+"さん");

			for(var i = 0 ; i < data.data.docs.length ; i++){
				var idtemp = data.data.docs[i].id['$t'];
				var temp =idtemp.replace('https://docs.google.com/feeds/id/','');
				var split = temp.split('%3');
				var type = split[0];
				var id = temp.replace(type+'%3','');
				if(type=='document'){
					$("#logs").append("<div id="+id+">"+data.data.docs[i].title['$t'] +"<a href="+data.data.docs[i].link[0].href+">[編集]</a><a href="+data.data.docs[i].content.src+">[orginal]</a><a href="+data.data.docs[i].content.src+"&exportFormat=doc&format=doc>[doc]</a><a href="+data.data.docs[i].content.src+"&exportFormat=html&format=html>[html]</a><a href="+data.data.docs[i].content.src+"&exportFormat=jpeg&format=jpeg[jpeg]</a><a href="+data.data.docs[i].content.src+"&exportFormat=odt&format=odt>[odt]</a><a href="+data.data.docs[i].content.src+"&exportFormat=pdf&format=pdf>[pdf]</a><a href="+data.data.docs[i].content.src+"&exportFormat=png&format=png>[png]</a><a href="+data.data.docs[i].content.src+"&exportFormat=rtf&format=rtf>[rtf]</a><a href="+data.data.docs[i].content.src+"&exportFormat=txt&format=txt>[txt]</a><a href="+data.data.docs[i].content.src+"&exportFormat=zip&format=zip>[zip]</a></div>");
				}else if(type=='presentation'){
					$("#logs").append("<div id="+id+">"+data.data.docs[i].title['$t'] +"<a href="+data.data.docs[i].link[0].href+">[編集]</a><a href="+data.data.docs[i].content.src+">[original]</a><a href="+data.data.docs[i].content.src+"&exportFormat=pdf>[pdf]</a><a href="+data.data.docs[i].content.src+"&exportFormat=png>[png]</a><a href="+data.data.docs[i].content.src+"&exportFormat=ppt>[ppt]</a><a href="+data.data.docs[i].content.src+"&exportFormat=txt>[txt]</a></div>");

				}else if(type=='spreadsheet'){

					$("#logs").append("<div id="+id+">"+data.data.docs[i].title['$t'] +"<a href="+data.data.docs[i].link[0].href+">[編集]</a><a href="+data.data.docs[i].content.src+">[original]</a><a href="+data.data.docs[i].content.src+"&exportFormat=xls>[xls]</a><a href="+data.data.docs[i].content.src+"&exportFormat=csv>[csv]</a><a href="+data.data.docs[i].content.src+"&exportFormat=pdf>[pdf]</a><a href="+data.data.docs[i].content.src+"&exportFormat=ods>[ods]</a><a href="+data.data.docs[i].content.src+"&exportFormat=tsv>[tsv]</a><a href="+data.data.docs[i].content.src+"&exportFormat=html>[html]</a></div>");
				}
				else if(type=='drawing'){
					$("#logs").append("<div id="+id+">"+data.data.docs[i].title['$t'] +"<a href="+data.data.docs[i].link[0].href+">[編集]</a><a href="+data.data.docs[i].content.src+">[original]</a><a href="+data.data.docs[i].content.src+"&exportFormat=jpeg>[jpeg]</a><a href="+data.data.docs[i].content.src+"&exportFormat=pdf>[pdf]</a><a href="+data.data.docs[i].content.src+"&exportFormat=png>[png]</a><a href="+data.data.docs[i].content.src+"&exportFormat=svg>[svg]</a></div>");

				}else{
					                         $("#logs").append("<div id="+id+">"+data.data.docs[i].title['$t'] +"<a href="+data.data.docs[i].link[0].href+">[見る]</a><a href="+data.data.docs[i].content.src+">[orginal]</a>");


											                         }


			}

		},
		jpegList:function(data){
			for(var i = 0 ; i < data.data.docs.length ; i++){
				var idtemp = data.data.docs[i].id['$t'];
				var temp =idtemp.replace('https://docs.google.com/feeds/id/','');
				var split = temp.split('%3');
				var type = split[0];
				var id = temp.replace(type+'%3','');
				
				var test = document.getElementById(id);
				var paths = "";
				if(data.data.docs[i].path!=undefined){
					for(var j = 0 ; j <data.data.docs[i].path.length;j++){
						paths = paths+"<a href=http://localhost/gsw2/examples/serv/jpeg/"+data.data.docs[i].title['$t']+"/"+data.data.docs[i].path[j]+">[jpeg"+j+"]</a>";
						if(j==data.data.docs[i].path.length-1){				
							$(test).append(paths);
						}
		
					}
				}
			}
		

		},
		auth_ok:function(data){
			$("#logs").text("認証が通って、ドキュメントリストを取得中");
		},   
		getCode:function(data){
			location.replace(data.url);
		},
		noData:function(data){
			$("#logs").text("ドキュメントがありません");
		}
	}

	function clear() {
		$("#message").val('').focus();
	}

});
